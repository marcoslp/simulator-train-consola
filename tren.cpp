#include "tren.h"

unsigned int Tren::getIdEst()
{
    return idEstacion;
}

void Tren::setIdEst(unsigned int value)
{
    idEstacion = value;
}

unsigned int Tren::getTiempoLlegadaDestino()
{
    return tiempoLlegadaDestino;
}

void Tren::setTiempoLlegadaDestino(unsigned int value)
{
    tiempoLlegadaDestino = value;
}

unsigned int Tren::getCantidadPasajerosBajan()
{
    return cantidadPasajerosBajan;
}

unsigned int Tren::getCantidadPersonasSuben()
{
    return cantidadPersonasSuben;
}

unsigned int Tren::getCantidadPasajeros()
{
    return cantidadPasajeros;
}

unsigned int Tren::getDireccion()
{
    return direccion;
}

void Tren::setDireccion(unsigned int value)
{
    direccion = value;
}

unsigned int Tren::getId()
{
    return id;
}

Tren::Tren(){}

Tren::Tren(unsigned int id, unsigned int cantidadPasajeros, unsigned int capacidad, unsigned int direccion, unsigned int idEstacion)
{
    this->id=id;
    this->idEstacion=idEstacion;
    this->cantidadPasajeros=cantidadPasajeros;
    this->capacidad=capacidad;
    this->direccion=direccion;
    this->tiempoLlegadaDestino=0;
    this->cantidadPasajerosBajan=0;
    this->cantidadPersonasSuben=0;
}
void Tren::subirPersonas(unsigned int personasEsperando)
{
    int espacioLibre=((this->capacidad)-(this->cantidadPasajeros));
    if(personasEsperando<=espacioLibre)
    {
        this->cantidadPersonasSuben=personasEsperando;
    }
    else
    {
        personasEsperando=personasEsperando-espacioLibre;
        this->cantidadPersonasSuben=espacioLibre;
    }
    this->cantidadPasajeros+=this->cantidadPersonasSuben;
}

void Tren::bajarPasajeros(double porcentaje)
{
    this->cantidadPasajerosBajan=this->cantidadPasajeros*porcentaje;
    this->cantidadPasajeros=this->cantidadPasajeros-this->cantidadPasajerosBajan;
}
