#include "estacion.h"

Estacion::Estacion(){}

unsigned int Estacion::getId()
{
    return this->id;
}

unsigned int Estacion::getTiempoEstacionAnterior()
{
    return tiempoEstacionAnterior;
}
double Estacion::getPorcentajePersonasBajan()
{
    return this->porcentajePersonasBajan;
}

unsigned int Estacion::getTiempoEstacionSiguiente()
{
    return tiempoEstacionSiguiente;
}

unsigned int Estacion::getCantidadPersonasEsperando()
{
    return cantidadPersonasEsperando;
}

void Estacion::setTiempoEstacionAnterior(unsigned int value)
{
    tiempoEstacionAnterior = value;
}

void Estacion::setTiempoEstacionSiguiente(unsigned int value)
{
    tiempoEstacionSiguiente = value;
}

void Estacion::setId(unsigned int _id)
{
    this->id=_id;
}

void Estacion::setCantidadPersonasEsperando(unsigned int n_p)
{
    this->cantidadPersonasEsperando=n_p;
}

void Estacion::setPorcentajePersonasBajan(double p_p_b)
{
    this->porcentajePersonasBajan=p_p_b;
}
