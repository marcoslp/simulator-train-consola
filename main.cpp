#include <iostream>
#include "central.h"
#include <cstring>
#include <string>
#include <vector>

using namespace std;

void menu();
int main()
{
    //menu(tiempoSimulacion, capacidadMaxima);
    //Central centralPrincipal(tiempoSimulacion);
    menu();

    cout<<endl;
    cout<<endl;
    cout<<endl;
    cout<<"Simulator trenes 3.0-"<<endl;
    cout<<"-Marcos Rios-"<<endl;
    return 0;
}

void menu(){   //Menu de inicio
    int o=1,op;
    unsigned tS=0;
    unsigned capmax=0;
    string nombre;
    cout<<"Ingrese la fecha en la que se ejecuta esta simulacion"<<endl;
    getline(cin,nombre);
    nombre.append(".bin");
    Central centralPrincipal(tS);
    while(o!=0){
        cout<<"Ingrese el tiempo de simulacion : "<<endl;
        cout<<" 1 - 20 Minutos, 900 personas"<<endl;
        cout<<" 2 - 50 Minutos, 900 personas"<<endl;
        cout<<" 3 - 20 Minutos, 700 personas"<<endl;
        cout<<" 4 - 50 Minutos, 700 personas"<<endl;
        cout<<" 5 - Leer un Registro de Simulacion"<<endl;
        cout<<" 6 - Salir"<<endl;
        cin>>op;
        cout<<endl;
        switch(op){
        case 1:tS=20;
               capmax=900;
               break;
        case 2:tS=50;
              capmax=900;
               break;
        case 3:tS=20;
               capmax=700;
               break;
        case 4:tS=50;
               capmax=700;
               break;
        case 5:centralPrincipal.leerArchivo();
            break;
        case 6:o=0;
              break;
        }
        centralPrincipal.setTiempoSimulacion(tS);
        centralPrincipal.iniciar(capmax,nombre);
    }
}
