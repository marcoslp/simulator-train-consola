#include "central.h"
#include <iostream>
#include <iterator>
#include <cstring>
#include <fstream>
using namespace std;
struct Registro{
  unsigned int id_Tren;
  unsigned int id_Estacion;
  unsigned int cantidadPersonasBajan;
  unsigned int cantidadPersonasSuben;
  unsigned int cantidadPasajeros;
  unsigned int TiempoLlegadaDestino;
} typedef registroSimulacion ;

void Central::grabarArchivo(unsigned int id_T,string nombreRegistro){

  //creando archivo
  ofstream registro;
  //abriendo archivo

  registro.open(nombreRegistro.c_str(),ios::out|ios::app|ios::binary);

  if(registro.is_open()==true)
  {
  registroSimulacion Reg;
  Reg.id_Tren=trenes[id_T].getId();
  Reg.id_Estacion=trenes[id_T].getIdEst();
  Reg.cantidadPersonasBajan=trenes[id_T].getCantidadPasajerosBajan();
  Reg.cantidadPersonasSuben=trenes[id_T].getCantidadPersonasSuben();
  Reg.cantidadPasajeros=trenes[id_T].getCantidadPasajeros();
  Reg.TiempoLlegadaDestino=trenes[id_T].getTiempoLlegadaDestino();

  registro.write((char*)&Reg,sizeof (Registro));
  registro.close();
  }
  else{
      cout<<"Ah ocurrido un error , al abrir el archivo."<<endl;
    }
}

void Central::leerArchivo()
{
  Registro reg;
  long tamanio;
  string nombre;
  cout<<"Ingrese la fecha del archivo a revisar:"<<endl;
  cin.get();
  getline(cin,nombre);
  nombre.append(".bin");
  ifstream archivo (nombre.c_str(),ios::in|ios::binary);
  tamanio=archivo.tellg()/int(sizeof (Registro));
  for(int i=0;i<tamanio;i++)
    {
      archivo.read((char*)&reg,sizeof (Registro));
      cout<<"Esta Mostrando los datos leidos"<<endl;
      cout<<"N° estacion: "<<reg.id_Estacion<<endl;
      cout<<"N° tren: "<<reg.id_Tren<<endl;
      cout<<"Pasajeros que bajan: "<<reg.cantidadPersonasBajan<<endl;
      cout<<"Pasajeros que suben: "<<reg.cantidadPersonasSuben<<endl;
      cout<<"Pasajeros: "<<reg.cantidadPasajeros<<endl;
      cout<<"Tiempo: "<<reg.TiempoLlegadaDestino<<endl;
      cout<<endl<<endl;
    }
  archivo.close();
}

void Central::setTiempoSimulacion(unsigned int value)
{
  tiempoSimulacion = value;
}

Central::Central(unsigned int tiempoSimulacion){
  this->cantidadTrenes=0;
  this->tiempoSimulacion=tiempoSimulacion;
  cargarEstaciones();
}

void Central::agregarTren(Tren tren)
{
    this->trenes.push_back(tren);
    cantidadTrenes++;
}

void Central::informar(unsigned int id_t)
{
   cout<<"N° estacion: "<<trenes[id_t].getIdEst()<<endl;
   cout<<"N° tren: "<<trenes[id_t].getId()<<endl;
   cout<<"Pasajeros que bajan: "<<trenes[id_t].getCantidadPasajerosBajan()<<endl;
   cout<<"Pasajeros que suben: "<<trenes[id_t].getCantidadPersonasSuben()<<endl;
   cout<<"Pasajeros: "<<trenes[id_t].getCantidadPasajeros()<<endl;
   cout<<"Tiempo: "<<trenes[id_t].getTiempoLlegadaDestino()<<endl;
   cout<<endl<<endl;
}

void Central::cargarEstaciones()
{
    for(unsigned int i=0;i<10;i++){
        Estacion e;
        e.setId(i);
        e.setCantidadPersonasEsperando(0);
        double p;
        switch (i) {
        case 0:
            p=1;
            break;
        case 9:
            p=1;
            break;
        case 4:
            p=0.40;
            break;
        case 5:
            p=0.40;
            break;
        default:
            p=0.20;
            break;
        }
        e.setPorcentajePersonasBajan(p);
        e.setTiempoEstacionAnterior(i);
        e.setTiempoEstacionSiguiente(i+1);
        estaciones.push_back(e);
    }
}

void Central::iniciar(unsigned int capacidadMaxima, string nombre){
    Tren t1(1,0,capacidadMaxima,9,0);
    Tren t2(2,0,capacidadMaxima,0,9);
    this->agregarTren(t1);
    this->agregarTren(t2);
    unsigned int TiempoAcumulado[2];
    TiempoAcumulado[0]=0;
    TiempoAcumulado[1]=0;
   unsigned int tiempo=0;
    cout<<"informe"<<endl<<endl;
    while(tiempoSimulacion != tiempo){
        for(unsigned int i=0;i<cantidadTrenes;i++)
        {

            //el tiempo acumulado en la clase tren es el T_cont que era vector estatico de el codigo viejo
            if(trenes[i].getDireccion()==9)
              {
                TiempoAcumulado[i]=(estaciones[trenes[i].getIdEst()].getTiempoEstacionSiguiente()+trenes[i].getTiempoLlegadaDestino());
              }
            else
              {
                TiempoAcumulado[i]=(estaciones[trenes[i].getIdEst()].getTiempoEstacionAnterior()+trenes[i].getTiempoLlegadaDestino());
              }
            if(TiempoAcumulado[i]==tiempo)
              {
                if(trenes[i].getDireccion()==9)
                  {
                    trenes[i].setIdEst(trenes[i].getIdEst()+1);
                  }
                else
                  {
                    trenes[i].setIdEst(trenes[i].getIdEst()-1);
                  }
                if((trenes[i].getIdEst()==9)&&(trenes[i].getDireccion()==9))
                  {
                    trenes[i].setDireccion(0);
                  }
                if((trenes[i].getIdEst()==0)&&(trenes[i].getDireccion()==0))
                  {
                    trenes[i].setDireccion(9);
                  }
                    trenes[i].setTiempoLlegadaDestino(tiempo);
                    trenes[i].bajarPasajeros(estaciones[trenes[i].getIdEst()].getPorcentajePersonasBajan());
                    trenes[i].subirPersonas(estaciones[trenes[i].getIdEst()].getCantidadPersonasEsperando());

                    grabarArchivo(i,nombre);
                    informar(i);
              }

}
        tiempo++;

        for(unsigned int i=0;i<10;i++){
            unsigned int p=estaciones[i].getCantidadPersonasEsperando()+20;
            estaciones[i].setCantidadPersonasEsperando(p);
        }
    }

}
