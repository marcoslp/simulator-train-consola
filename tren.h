#ifndef TREN_H
#define TREN_H
#include<estacion.h>

class Tren
{
private:
    unsigned int id;                     //indentificador del tren
   unsigned int idEstacion;             //indentificador de la estacion donde esta el tren
    unsigned int cantidadPasajeros;      //numero de pasajeros arriba del tren
    unsigned int capacidad;              //capacidad maxima del tren
    unsigned int cantidadPasajerosBajan;  //cantidad de personas que bajan en cada estacion
    unsigned int cantidadPersonasSuben;  //cantidad de personas que suben en cada estacion
    unsigned int direccion;              //direccion en la que se dirije el tren ->9 / ->0
    unsigned int tiempoLlegadaDestino;   //minuto en el que el tren llego a la estacion de destino

public:
    Tren();
    Tren(unsigned int , unsigned int,unsigned int,unsigned int,unsigned int); // TODO
    void subirPersonas(unsigned int);  //actualiza(sube) la cantidad de personas en el tren y los que estan en espera
    void bajarPasajeros(double);//actualiza(baja) la cantidad de personas que se encuentran en el tren y los que estan en espera según el porcentaje correspondiente
    unsigned int getId();
    unsigned int getIdEst();
    unsigned int getTiempoLlegadaDestino();
    unsigned int getCantidadPasajerosBajan();
    unsigned int getCantidadPersonasSuben();
    unsigned int getCantidadPasajeros();
    unsigned int getDireccion();
    unsigned int getTiempoAcumulado();
    void setIdEst(unsigned int);
    void setTiempoLlegadaDestino( unsigned int);
    void setDireccion(unsigned int);
    void setTiempoAcumulado(unsigned int);

};

#endif // TREN_H
