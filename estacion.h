#ifndef ESTACION_H
#define ESTACION_H

class Estacion
{
private:
    unsigned int id;                             //identificador de estacion
    double porcentajePersonasBajan;     //porcentaje de personas que deben bajar
    unsigned int cantidadPersonasEsperando;      //personas que esperan
    unsigned int tiempoEstacionAnterior;         //tiempo a la estacion anterior
    unsigned int tiempoEstacionSiguiente;        //tiempo a la estacion siguiente
public:
    Estacion();
    Estacion( unsigned int ,double,unsigned int);
    void setId(unsigned int);
    void setCantidadPersonasEsperando(unsigned int);
    void setPorcentajePersonasBajan(double);
    void setTiempoEstacionAnterior(unsigned int value);
    void setTiempoEstacionSiguiente( unsigned int value);
    unsigned int getId();
    unsigned int getTiempoEstacionAnterior();
   unsigned int getTiempoEstacionSiguiente();
    double getPorcentajePersonasBajan();
    unsigned int getCantidadPersonasEsperando();

};

#endif // ESTACION_H
