TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        central.cpp \
        estacion.cpp \
        main.cpp \
        tren.cpp

HEADERS += \
    central.h \
    estacion.h \
    tren.h
