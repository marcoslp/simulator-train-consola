#ifndef CENTRAL_H
#define CENTRAL_H
#include "tren.h"
#include "estacion.h"
#include <iostream>
#include <vector>
using namespace std;
class Central
{
private:
    unsigned int tiempoSimulacion;//tiempo de simulacion
    vector<Tren> trenes;
    vector<Estacion> estaciones;
    unsigned int cantidadTrenes;

public:

    Central( unsigned int);
    void iniciar(unsigned int,string);
    void cargarEstaciones();
    void agregarTren(Tren);
    void informar(unsigned int);
    void grabarArchivo(unsigned int, string);
    void leerArchivo();
    //esto va privado pero es para probar
    void setTiempoSimulacion(unsigned int value);
};

#endif // CENTRAL_H
